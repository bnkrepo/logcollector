﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Ionic.Zip;
using LogCollector.Properties;

namespace LogCollector
{
    public partial class MainForm : Form
    {
        delegate void AddToListDelegate(ListViewItem item);
        delegate void SetProgressDelegate(string text);
        delegate void EnableOpenButtonDelegate(bool bEnable);
        delegate void EnableReportButtonDelegate(bool bEnable);
        delegate string GetUserNotesDelegate();
        delegate string GetAttachmentPathDelegate();

        string _logFolder = "";
        string _reportFolder = "";
        string _currentPath = "";

        bool bReportGenerated = false;

        List<string> _sourceNames = new List<string>();

        public MainForm()
        {
            InitializeComponent();

            this.Text = Settings.Default.ApplicationName + " Log Collector";
            lblHeader.Text = "Unfortunately an error occurred while running " + Settings.Default.ApplicationName + " and it has to be stopped as a result of an unhandled exception.";
            _logFolder = Settings.Default.LogPath;
            string[] args = Settings.Default.CommandLine.Split(',');

            for (int i = 0; i < args.Length; i++)
            {
                if (string.IsNullOrEmpty(args[i]) == true)
                    continue;

                _sourceNames.Add(args[i].Trim());
            }            
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Event handler. Called by MainForm for load events. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        ///-------------------------------------------------------------------------------------------------

        private void MainForm_Load(object sender, EventArgs e)
        {
        }

        /// <summary>   Collect log data. </summary>
        private void CollectLogData()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            _currentPath = Path.GetDirectoryName(assembly.Location);

            //Get Events
            DateTime yesterday = DateTime.Now.AddDays(-1);
            EventLog eventLog = new EventLog();
            eventLog.Log = "Application";
            List<string> logs = new List<string>();
            List<string> systemInfo = new List<string>();
            

            SetProgress("Reading application log entries...");

            foreach (EventLogEntry entry in eventLog.Entries)
            {
                foreach (string source in _sourceNames)
                {
                    if (entry.Source.CompareTo(source) == 0 && entry.TimeGenerated.Date == DateTime.Today)
                    {
                        if (systemInfo.Count == 0)
                        {
                            systemInfo.Add("Machine Name: " + entry.MachineName);
                            systemInfo.Add("Source: " + entry.Source);
                        }
                        else
                        {
                            logs.Add("Time Generated: " + entry.TimeGenerated.ToString());
                            logs.Add("Message: ");
                            logs.Add(entry.Message);
                        }
                    }
                }
            }

            SetProgress("Generating output for application log...");

            // Dump to a file
            string timeStamp = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
            string eventFile = _currentPath + "\\" + "Windows Logs.Application_" + timeStamp + ".txt";
            using (StreamWriter writer = new StreamWriter(eventFile))
            {
                foreach (string line in systemInfo)
                {
                    writer.WriteLine(line);
                }

                writer.WriteLine("");

                int newLineCounter = 0;

                foreach (string line in logs)
                {
                    writer.WriteLine(line);
                    newLineCounter++;

                    if (newLineCounter >= 3)
                    {
                        writer.WriteLine("");
                        newLineCounter = 0;
                    }
                }

                writer.Close();
            }

            SetProgress("Compressing GUI logs...");

            // Zip GUI Logs
            string logZipFile = _currentPath + "\\Logs_" + timeStamp + ".zip";
            string logFilePath = _currentPath + "\\" + _logFolder;

            if (Directory.Exists(logFilePath) == true)
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    zip.AddDirectory(logFilePath);
                    zip.Comment = "";
                    zip.Save(logZipFile);
                }
            }

            SetProgress("Generating report folder...");

            _reportFolder = _currentPath + "\\Reports";
            string currentReportFolder = _reportFolder + "\\" + Settings.Default.ApplicationName + "_Report_" + timeStamp;            

            // Create report folder
            if(Directory.Exists(_reportFolder) == false)
            {
                Directory.CreateDirectory(_reportFolder);
            }

            if (Directory.Exists(currentReportFolder) == false)
            {
                Directory.CreateDirectory(currentReportFolder);
            }

            SetProgress("Moving files...");

            if (File.Exists(eventFile) == true)
                File.Move(eventFile, currentReportFolder + "\\" + Path.GetFileName(eventFile));

            if(File.Exists(logZipFile) == true)
                File.Move(logZipFile, currentReportFolder + "\\" + Path.GetFileName(logZipFile));

            // Get GUI and all assembly information
            string assemblyInfoFile = DumpGUIAssemblyInformation(timeStamp, currentReportFolder);
            // Generate system information
            string systemInfoFile = DumpSystemInformation(timeStamp, currentReportFolder);
            
            SetProgress("Saving user notes...");

            string userNotes = GetUserNotes();
            string userNoteFile = "";

            if (string.IsNullOrEmpty(userNotes) == false)
            {
                userNoteFile = currentReportFolder + "\\" + "UserNotes.txt";

                using (StreamWriter writer = new StreamWriter(userNoteFile))
                {
                    writer.Write(userNotes);
                    writer.Close();
                }
            }

            SetProgress("Saving attachment...");

            string attachmentPath = GetAttachmentPath();
            string attachmentFile = "";

            if (string.IsNullOrEmpty(attachmentPath) == false && File.Exists(attachmentPath) == true)
            {
                attachmentFile = currentReportFolder + "\\User_Attachment_" + Path.GetFileName(attachmentPath);
                File.Move(attachmentPath, attachmentFile);
            }

            SetProgress("Compressing report folder...");

            // Zip report folder
            string currentReportFolderZip = currentReportFolder + ".zip";

            if(Directory.Exists(currentReportFolder) == true)
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    zip.AddDirectory(currentReportFolder);
                    zip.Comment = "";
                    zip.Save(currentReportFolderZip);
                }
            }
            

            // Show Details.
            ListViewItem item = new ListViewItem(Path.GetFileName(eventFile));
            item.SubItems.Add("Windows Event Logs for " + Settings.Default.ApplicationName);
            AddToList(item);

            item = new ListViewItem(Path.GetFileName(logZipFile));
            item.SubItems.Add(Settings.Default.ApplicationName + " Log files");
            AddToList(item);

            item = new ListViewItem(Path.GetFileName(assemblyInfoFile));
            item.SubItems.Add(Settings.Default.ApplicationName + " assembly file information");
            AddToList(item);

            item = new ListViewItem(Path.GetFileName(systemInfoFile));
            item.SubItems.Add("Current system information");
            AddToList(item);

            if (string.IsNullOrEmpty(userNoteFile) == false && File.Exists(userNoteFile) == true)
            {
                item = new ListViewItem(Path.GetFileName(userNoteFile));
                item.SubItems.Add("User Notes");
                AddToList(item);
            }

            if (string.IsNullOrEmpty(attachmentFile) == false && File.Exists(attachmentFile) == true)
            {
                item = new ListViewItem(Path.GetFileName(attachmentFile));
                item.SubItems.Add("User Attachment");
                AddToList(item);
            }

            SetProgress("Cleaning up...");

            // Remove original report folder after it's been zipped.
            if (Directory.Exists(currentReportFolder) == true)
                Directory.Delete(currentReportFolder, true);

            EnableOpenButton(listView.Items.Count > 0);

            SetProgress(string.Format("Please send above data to {0} for investigating this issue.", Settings.Default.CompanyName));
            bReportGenerated = true;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Dumps a system information. </summary>
        ///
        /// <param name="timeStamp">            The time stamp. </param>
        /// <param name="currentReportFolder">  Pathname of the current report folder. </param>
        ///
        /// <returns>   . </returns>
        ///-------------------------------------------------------------------------------------------------

        private string DumpSystemInformation(string timeStamp, string currentReportFolder)
        {
            string systemInfoFile = currentReportFolder + "\\SystemInfo_" + timeStamp + ".txt";
            string sysinfoBat = _currentPath + "\\sysinfo.bat";

            try
            {                
                using (StreamWriter writer = new StreamWriter(sysinfoBat))
                {
                    writer.WriteLine("systeminfo > \"" + systemInfoFile + "\"");
                    writer.WriteLine("tasklist >> \"" + systemInfoFile + "\"");
                    writer.Close();
                }

                if (File.Exists(sysinfoBat) == true)
                {
                    SetProgress("Generating system information...");

                    var process = new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            FileName = sysinfoBat
                        }
                    };

                    process.Start();
                    process.WaitForExit();

                }
            }
            catch (Exception)
            {

            }
            finally
            {
                if (File.Exists(sysinfoBat) == true)
                    File.Delete(sysinfoBat);
            }

            return systemInfoFile;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Dumps a graphical user interface assembly information. </summary>
        ///
        /// <param name="timeStamp">            The time stamp. </param>
        /// <param name="currentReportFolder">  Pathname of the current report folder. </param>
        ///
        /// <returns>   . </returns>
        ///-------------------------------------------------------------------------------------------------

        private string DumpGUIAssemblyInformation(string timeStamp, string currentReportFolder)
        {
            List<string> files = new List<string>();

            SetProgress("Generating list of assemblies...");

            foreach (string file in Directory.EnumerateFiles(_currentPath, "*.*",
                      SearchOption.AllDirectories).Where(s => s.EndsWith(".exe") || s.EndsWith(".dll")))
            {
                files.Add(file);
            }

            string assemblyInfoFile = currentReportFolder + "\\Assemblies_" + timeStamp + ".txt";

            SetProgress("Reading assembly information...");

            using (StreamWriter writer = new StreamWriter(assemblyInfoFile))
            {
                int index = 0 ;
                string line = "Files: ";
                writer.WriteLine(line);
                writer.WriteLine("");

                foreach (string file in files)
                {
                    SetProgress(Path.GetFileName(file));

                    try
                    {
                        index++;
                        Assembly assembly = Assembly.LoadFrom(file);
                        line =  string.Format("{0,-4}{1,-50}{2,-20}", index.ToString() + ". ", Path.GetFileName(file), assembly.GetName().Version.ToString());
                    }
                    catch (Exception)
                    {
                        line = "Error in getting version information for '" + file + "'"; ;
                    }

                    writer.WriteLine(line);
                }

                writer.WriteLine("");

                writer.Close();
            }

            return assemblyInfoFile;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if(bReportGenerated == false)
            {
                if (MessageBox.Show("Error report is not generated. Do you want to cancel this operation?", 
                                    "Log Collector", MessageBoxButtons.YesNo,  MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
            }

            this.Close();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                if (Directory.Exists(_reportFolder) == true)
                    Process.Start(_reportFolder);
            }
            catch (Exception)
            {

            }
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Adds to the list. </summary>
        ///
        /// <param name="item"> The item. </param>
        ///-------------------------------------------------------------------------------------------------

        public void AddToList(ListViewItem item)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new AddToListDelegate(this.AddToList), new object[] { item });
            }
            else
            {
                listView.Items.Add(item);
            }
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Sets the progress. </summary>
        ///
        /// <param name="text"> The text. </param>
        ///-------------------------------------------------------------------------------------------------

        public void SetProgress(string text)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new SetProgressDelegate(this.SetProgress), new object[] { text });
            }
            else
            {
                lblProgress.Text = text;
            }
        }

        public void EnableOpenButton(bool bEnable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new EnableOpenButtonDelegate(this.EnableOpenButton), new object[] { bEnable });
            }
            else
            {
                btnOpen.Enabled = bEnable;
            }
        }

        public void EnableReportButton(bool bEnable)
        {
            if (this.InvokeRequired == true)
            {
                this.Invoke(new EnableReportButtonDelegate(this.EnableReportButton), new object[] { bEnable });
            }
            else
            {
                btnGenerateReport.Enabled = bEnable;
            }
        }

        public string GetUserNotes()
        {
            if (this.InvokeRequired == true)
            {
               return (string ) this.Invoke(new GetUserNotesDelegate(this.GetUserNotes), new object[] { });
            }
            else
            {
                return txtUserNotes.Text;
            }
        }

        public string GetAttachmentPath()
        {
            if (this.InvokeRequired == true)
            {
                return (string)this.Invoke(new GetAttachmentPathDelegate(this.GetAttachmentPath), new object[] { });
            }
            else
            {
                return txtFilePath.Text;
            }
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            EnableReportButton(false);
            CollectLogData();
            EnableReportButton(true); 
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
        }

        private void txtFilePath_TextChanged(object sender, EventArgs e)
        {            
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFilePath.Text = openFileDialog.FileName;
            }
        }

        private void btnGenerateReport_Click(object sender, EventArgs e)
        {
            if (_sourceNames == null || _sourceNames.Count == 0)
                this.Close();

            backgroundWorker.RunWorkerAsync();
        }

    }
}
